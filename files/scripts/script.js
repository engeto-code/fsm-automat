const app = document.querySelector('#app');

class Produkt {
    nazev = ""
    cena = ""
    potrebujevodu = false

    constructor (nazev, cena, voda = false){
        if (typeof nazev != 'string' || nazev.length < 3){
            throw new Error("Zadej název aspoň 2 znaky");
        }
        if (typeof cena != 'number' || cena < 5){
            throw new Error("Cena je příliš nízká");
        }
        if (typeof voda != 'boolean'){
            throw new Error("ZLS");
        }

        this.nazev = nazev
        this.cena = cena
        this.potrebujevodu = voda
    }
}

class Automat {
    produkt = null;
    mince = 0;

    nabidka = null;

    constructor(nabidka){
        this.nabidka = nabidka;
    }

    vyberProdukt(piti, vyber){
        if (!this.nabidka){
            throw new Error('Nabidka je prazdna');
        }
        if (this.produkt !== null){
            throw new Error('Zaplať!');
        }

        if (piti === false){
            if (this.nabidka.jidlo[vyber] === undefined){
                throw new Error('Vyběr beexustzhe');
            }
            // vypíšeme do console
            console.log(this.nabidka.jidlo[vyber]);
            this.produkt = this.nabidka.jidlo[vyber];
        } else if (piti === true){
            if (this.nabidka.piti[vyber] === undefined){
                throw new Error('Vyběr beexustzhe');
            }
            // vypíšeme do console
            console.log(this.nabidka.piti[vyber]);
            this.produkt = this.nabidka.piti[vyber];
        }
    }

    vhodMinci(hodnota){
        if (![1,2,5,10,20].includes(hodnota)){
            console.error('Špatná mince');
            return;
        }

        // přičte hodnotu mince k zaplaceným mincím
        this.mince += hodnota;

        if (this.produkt.cena > this.mince){
            console.error('Zaplať');
        } else {
            this.vydejProdukt();
        }
    }

    vydejProdukt(){
        const vraceni = this.mince - this.produkt.cena;
        console.log('Vydávám ', this.produkt, 'vracim ', vraceni);
        // vynulovat stav
        this.produkt = null;
        this.mince = 0;
    }

    reset(){
        console.log('Vracim penize, stornuju objednavku', this.mince);
        this.produkt = null;
        this.mince = 0;
    }

}

try{
    const nabidka = {
        jidlo: [
            new Produkt("Tyčinky", 25),
            new Produkt("Bageta", 65),
        ],
        piti: [
            new Produkt("Kafe", 25),
            new Produkt("Limo", 30),
            new Produkt("Voda", 10),
            new Produkt("Čaj", 20),
        ]
    }

    const ATM = new Automat(nabidka);
    ATM.vyberProdukt(false, 1);
    ATM.vhodMinci(20);
    ATM.vhodMinci(20);
    ATM.vhodMinci(20);
    ATM.vhodMinci(10);
} catch (e){
    console.log(e);
}